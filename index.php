<?php

$search_words = ['php','html','Интернет','Web'];

$search_strings = ['Интернет - большая сеть компьютеров, которые могут
 взаимодействовать друг с другом.',
'PHP - это распространенный язык программирования  с открытым исходным кодом.',
'PHP сконструирован специально для ведения Web-разработок и его код может 
внедряться непосредственно в HTML'];


function Regular($search_words, $search_strings)
{
    $result = '';

    foreach ($search_strings as $key => $string) {
        $result .= '<p>' . 'В предложении №' . $key . ' есть слова: ';
        foreach ($search_words as $word) {

            $reg = '/(' . $word . ')+/i';

            if (preg_match($reg, $string)) {
                $result .= $word . ' ';
            }
        }
    }
    return $result;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>
        <?=Regular($search_words,$search_strings)?>
    </p>
</body>
</html>
